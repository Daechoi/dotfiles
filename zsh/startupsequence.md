interactive , *(interactive non-login), ** (script)

/etc/zshenv, *, **
~/.zshenv, *, **
/etc/zprofile
~/.zprofile
/etc/zshrc, *
~/.zshrc, *
/etc/zlogin
~/.zlogin
~/.zlogout 
/etc/zlogout


