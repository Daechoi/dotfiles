# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# https://github.com/zsh-users/zsh-completions
zstyle :compinstall filename '~/.zshrc'
autoload -Uz compinit
compinit

# source zpresto
source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
# source the blox prompt + customizations


#zstyle ':prezto:module:prompt' theme 'spaceship'
#source "${ZDOTDIR:-$HOME}/.config/zsh/prompt.zsh"
# https://github.com/zsh-users/zsh-autosuggestions
# source nvm as a zsh plugin (https://github.com/lukechilds/zsh-nvm)
source "${ZDOTDIR:-$HOME}/.config/zsh/zsh-nvm/zsh-nvm.plugin.zsh"

# custom config
source "${ZDOTDIR:-$HOME}/.config/zsh/aliases.zsh"
source "${ZDOTDIR:-$HOME}/.config/zsh/functions.zsh"
source "${ZDOTDIR:-$HOME}/.config/zsh/keybindings.zsh"
source "${ZDOTDIR:-$HOME}/.config/zsh/git.zsh"
source "${ZDOTDIR:-$HOME}/.config/zsh/docker.zsh"
source "${ZDOTDIR:-$HOME}/.config/zsh/autojump.zsh"
#source "/usr/local/opt/kube-ps1/share/kube-ps1.sh"
# PS1='$(kube_ps1)'$PS1
# override prezto and plugins here
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000

setopt appendhistory autocd extendedglob nomatch notify nosharehistory incappendhistory
unsetopt beep

prompt powerlevel10k
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(virtualenv dir vcs)
POWERLEVEL9K_VIRTUALENV_BACKGROUND='springgreen4'

case `uname` in
  Darwin)
source /usr/local/share/chruby/chruby.sh

eval "$(pyenv init -)"
#source ~/.pyenv/versions/3.8.2/bin/virtualenvwrapper.sh
export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"
export WORKON_HOME=$HOME/.virtualenvs
#pyenv virtualenvwrapper_lazy

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/terraform terraform
# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
#[[ -f /Users/dchoi/playGround/js_cloud/cncb-create-stack/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/dchoi/playGround/js_cloud/cncb-create-stack/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
#[[ -f /Users/dchoi/playGround/js_cloud/cncb-create-stack/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/dchoi/playGround/js_cloud/cncb-create-stack/node_modules/tabtab/.completions/sls.zsh
#export PATH="/usr/local/opt/libpq/bin:$PATH"
#export DYLD_LIBRARY_PATH=/usr/local/opt/libpq/lib:/usr/local/Cellar/openssl@1.1/1.1.1j/lib
export DYLD_LIBRARY_PATH=${HOME}/build/pginst/lib:/usr/local/Cellar/openssl@1.1/1.1.1j/lib

source "${ZDOTDIR:-$HOME}/.config/zsh/fzf.zsh"
;;
Linux)
  #commands for Linux
  #
  export PATH="/home/dchoi/.pyenv/bin:$PATH"
  eval "$(pyenv init -)"
  eval "$(pyenv virtualenv-init -)"

  ;;
esac

export TERM=xterm-256color-italic
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

source "${ZDOTDIR:-$HOME}/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "${ZDOTDIR:-$HOME}/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export LDFLAGS="-L/usr/local/opt/zlib/lib -L/usr/local/opt/bzip2/lib -L/usr/local/opt/flex/lib -L/usr/local/opt/openssl@3/lib -L/usr/local/opt/lz4/lib -L/usr/local/opt/zstd/lib"
export CPPFLAGS="-I/usr/local/opt/zlib/include -I/usr/local/opt/bzip2/include -I/usr/local/opt/flex/include -I/usr/local/opt/openssl@3/include -I/usr/local/opt/lz4/include -I/usr/local/opt/zstd/include -I/${HOME}/.pgx/15.3/src/include"

alias dc=docker-compose

alias ia="open $1 -a /Applications/iA\ Writer.app"
alias la="ls -al"
alias cdd="cd /Users/dchoi/Library/Mobile\ Documents/27N4MQEA55\~pro\~writer/Documents"
alias cdp="cd ~/Projects/sif/Vanir"
alias cdc="cd ~/Projects/crystal-cloud"
alias k="kubectl"

export GOPATH=$HOME/gocode
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/usr/local/Cellar/perl/5.34.0_1/bin
export PATH=$PATH:/usr/local/opt/flex/bin
export PATH=$PATH:/usr/local/opt/openssl@3/bin


source <(kubectl completion zsh)
export CFS_HOME=~/Projects/skunkworks/cfs
export CFS_LIB=${CFS_HOME}/build/Debug/lib
export CFS_INC=${CFS_HOME}/include
export CC=/usr/local/bin/gcc-12
export CXX=/usr/local/bin/g++-12
export PATH="/usr/local/opt/llvm/bin:$PATH"

#export PATH="${HOME}/.pgrx/15.3/pgrx-install/bin:$PATH"
export kindport=36915
export KUBECONFIG=~/.kube/config

export sandbox=daesand
export username=$(whoami)
source <(kubectl completion zsh)

export PATH="/usr/local/opt/libpq/bin:$PATH"
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Projects

source <(kubebuilder completion zsh)
