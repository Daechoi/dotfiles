--[[
Neovim init file
Maintainer:  daedalus
Website: https://github.com/daechoi/neovim-lua

--]]

-- Import Lua modules

require('packer_init')

require('core/keymaps')
require('core/colors')
require('core/statusline')
require('core/options')

require('plugins/nvim-lightbulb')
require('plugins/nvim-tree')
require('plugins/indent-blankline')

require('plugins/nvim-lspconfig')
require('plugins/rust-tools')
require('plugins/nvim-cmp')
require('plugins/nvim-treesitter')
require('plugins/alpha-nvim')
require('plugins/vgit')
require('plugins/vscode-js-debug')
require('plugins/format')

