-----------------------------------------------------------------------------------------
-- General Neovim settings and configuration
-----------------------------------------------------------------------------------------

local g = vim.g 			-- global variables
local opt = vim.opt			-- Set options (global/buffer/windows-scoped)

-----------------------------------------------------------------------------------------
-- General
-----------------------------------------------------------------------------------------

opt.mouse = 'a'				-- Enable mouse support
opt.clipboard = 'unnamedplus'		-- Copy/paste to system clipboard

-----------------------------------------------------------------------------------------
-- Neovim UI
-----------------------------------------------------------------------------------------

opt.number = true			-- Show line number
opt.laststatus = 3			-- Set global statusline
opt.colorcolumn = '100'			-- Line length marker at 100 columns
opt.termguicolors = true		-- Enable 24-bit RGB colors
opt.ignorecase = true			-- Ignore case letters when searching
opt.wrap = false

-----------------------------------------------------------------------------------------
-- Tabs, indent
-----------------------------------------------------------------------------------------

opt.expandtab = true			-- Use spaces instead of tabs
opt.tabstop = 	4			-- 1 tab == 4 spaces
opt.shiftwidth = 4			-- Shift 2 spaces when tab
opt.softtabstop = 4			-- Shift 2 spaces when tab
opt.smartindent = true			-- Autoindent new lines
opt.expandtab = true			-- Autoindent new lines
opt.autoindent = true			-- Autoindent new lines

-----------------------------------------------------------------------------------------
-- Memory, CPU
-----------------------------------------------------------------------------------------

opt.lazyredraw = true			-- Faster scrolling
