-----------------------------------------------------------
-- Plugin manager configuration file
-----------------------------------------------------------

-- Plugin manager: packer.nvim
-- url: https://github.com/wbthomason/packer.nvim

-- Automatically install packer
--
local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({
    'git',
    'clone',
    '--depth',
    '1',
    'https://github.com/wbthomason/packer.nvim',
    install_path
  })
  vim.o.runtimepath = vim.fn.stdpath('data') .. '/site/pack/*/start/*,' .. vim.o.runtimepath
end

-- Autocommand that reloads neovim whenever you save the packer_init.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost packer_init.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, 'packer')
if not status_ok then
  return
end

-- Install plugins
return packer.startup(function(use)
  -- Add you plugins here:
  use 'wbthomason/packer.nvim' -- packer can manage itself

  -- File explorer
  use 'kyazdani42/nvim-tree.lua'


  -- Indent line
  -- shows the indentation vertical guide lines for tabs
  use 'lukas-reineke/indent-blankline.nvim'

  -- Telescope
  -- fuzzy finder over lists
  use {
    'nvim-telescope/telescope.nvim',
    main = "ibl",
    opts = {}
  }


  -- Autopair
  -- Adds brackets
  use {
    'windwp/nvim-autopairs',

    config = function()
      require('nvim-autopairs').setup{}
    end
  }

  -- Icons
  -- lua fork of vim-devicons
  use 'kyazdani42/nvim-web-devicons'

  -- Tag viewer
  -- generates tags of currently open file.  use leader z
  use 'preservim/tagbar'

  -- Treesitter interface
  -- parser generator tool with concrete syntax tree for sourcefile
  -- and updates the syntax tree as sourcefile is edited.
  --
  use {
    'nvim-treesitter/nvim-treesitter',
    run = function() require('nvim-treesitter.install').update({ with_sync = true }) end,
  }

  -- LSP
  -- language server protocol.  Client to LSP servers
  -- go-to-definition, find-references, hover, completion,
  -- rename, format, refactor, using semantic whole-project analysis
  -- unlike ctags
  use 'neovim/nvim-lspconfig'

  -- Autocomplete
  use ("hrsh7th/nvim-cmp")
  use({
      -- cmp LSP completion
      "hrsh7th/cmp-nvim-lsp",
      -- cmp Snippet completion
      "hrsh7th/cmp-vsnip",
      -- cmp Path completion
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-buffer",
      after = {"hrsh7th/nvim-cmp"},
      requires = {"hrsh7th/nvim-cmp"}
  })
  -- See hrsh7th other plugins for more great completion sources
  -- Snippet engine

  use ("hrsh7th/vim-vsnip")
  -- Optional

  use ("nvim-lua/popup.nvim")
  use ("nvim-lua/plenary.nvim")
  -- Statusline
  -- Bottom status line bar
  use {
    'feline-nvim/feline.nvim',
    requires = { 'kyazdani42/nvim-web-devicons' },
  }

  -- git labels
  use {
    'lewis6991/gitsigns.nvim',
    requires = { 'nvim-lua/plenary.nvim' },
    config = function()
      require('gitsigns').setup{}
    end
  }

  -- Dashboard (start screen)
  use {
    'goolord/alpha-nvim',
    requires = { 'kyazdani42/nvim-web-devicons' },
  }

  -- For Rust
  use 'simrat39/rust-tools.nvim'
  use 'mfussenegger/nvim-dap'

  use { "rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"} }

  use {
    "mxsdev/nvim-dap-vscode-js", requires = {"mfussenegger/nvim-dap"}
  }


use "IndianBoy42/tree-sitter-just"
--  use {
 --   "microsoft/vscode-js-debug",
 --   opt = true,
--    run = "npm install --legacy-peer-deps && npm run compile"
  --}

  use {
    'tanvirtin/vgit.nvim',
    requires = { 'nvim-lua/plenary.nvim'}
  }

--  use('jose-elias-alvarez/null-ls.nvim')
 -- use('MunifTanjim/prettier.nvim')
  use {"mhartington/formatter.nvim"}
  use {
      'kosayoda/nvim-lightbulb',
      requires = 'antoinemadec/FixCursorHold.nvim',
  }
  -- Color schemes
  use 'navarasu/onedark.nvim'


  use {'iamcco/markdown-preview.nvim', run = "cd app && npm install", setup = function() vim.g.mkdp_filetypes = { "markdown" } end, ft = { "markdown" }, }
  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins

use {'github/copilot.vim'}

  if packer_bootstrap then
    require('packer').sync()
  end
end)
