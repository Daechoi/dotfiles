function format_node_prettier()
   return {
     exe = "prettier",
     args = {"--stdin-filepath", vim.api.nvim_buf_get_name(0)},
     stdin = true
   }
end


require('formatter').setup {
  logging = true,
  filetype = {
    typescript = { format_node_prettier },
    typescriptreact =  { format_node_prettier },
    javascript = { format_node_prettier },
    javascriptreact = { format_node_prettier },
    json = { format_node_prettier },
    -- will check for other language support
    -- autosave support
  }
}


